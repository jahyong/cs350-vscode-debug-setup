# CS350 VS Code Debug Setup

This repo allows you to use VS Code's debugger with `cs350-gdb`. 

- [Prerequisites](#prerequisites)
- [Why is this useful?](#why-is-this-useful)
- [How do I use it?](#how-do-i-use-it)
- [What does the script do?](#what-does-the-script-do)
- [What can I customize?](#what-can-i-customize)
- [Things that might go wrong](#things-that-might-go-wrong)
- [Things I might do](#things-i-might-do)
- [Why is there a license?](#why-is-there-a-license)


# Prerequisites
- You are working locally with the instructor provided [docker container repo](https://git.uwaterloo.ca/krhancoc/cs350-container)
- You are using VS Code
- You have the VS Code Docker extension installed
- You have bash (linux or mac or WSL probably works)

<details>
<summary>Probably Mac OS specific:</summary>

TLDR: `brew install gettext wget bash && brew link --force gettext`

- You have the command `envsubst`
    - If not, on Ubuntu you can install with `sudo apt install gettext`
    - On Mac, install it with 
    ```bash
    brew install gettext
    brew link --force gettext 
    ``` 
- You have the command `wget`
    - Yes, I know, `curl` works, but it's not in the script right now, so just `brew install wget`
- Your bash version is 4.0 or above
    - By default, I think MacOS only distributes with Bash 3.2 since that's the last non-GPL version, but you can still get the newer version with `brew install bash`
- Your `cs350-*.tar.gz` in `cs350-container/os161-container/dependencies` are physically on your machine. Sometimes iCloud puts them into the cloud, and you only have a (not useful) `.icloud` reference file. 


</details>


# Why is this useful?
1. You can more easily see the context of your code, hover over variables to see their value, and create breakpoints without having to search for the line number. CLI gdb is also kind of hard. 
2. You can build and start a kernel (debug or not) while staying in VS Code, so you don't have to type out the command to start sys161 every time. 

# How do I use it?
```bash
git clone ist-git@git.uwaterloo.ca:a267chen/cs350-vscode-debug-setup.git && cd cs350-vscode-debug-setup
./setup.sh <path_to_your_os161-1.99_folder> <path_to_your_cs350-container_folder> <path_to_your_vscode_workspace_root> [ASSTX]
```

You may want to configure it when you start working on a different assignment. 

To:
- start the container (like `run.sh` without being stuck in the shell)
    - run the VS Code command (ctrl-shift-P) `Docker Compose Up`
- create a shell (like `connect.sh`)
    - run the VS Code command `Docker Containers: Attach Shell` and navigate to the appropriate container (probably something with the word OS in it); OR
    - in a terminal, navigate to your VS Code workspace (wherever the `docker-compose.yml` is) and run `docker-compose exec os161-runner bash`
- start the container (in the background) AND build the kernel (for assignment `ASSTX`)
    - run ctrl-shift-B (!)
- debug in VS Code
    - create a shell, start a debugger session (`sys161 -w os-compile/kernel-ASSTX ...`)
    - press F5 in VS Code
- run a test (all of them, specific assignment test, custom command)
    - run the VS Code Command `Tasks: Run Test Task`
    - follow the instructions from there

# What does the script do?
- Updates the os161 gdb to a new version, because the one supplied by the instructors (6.1) is a bit too old to communicate with VS Code
- Substitutes your os161-1.99 and cs350-container paths into the `docker-compose.yml` and `.json` files and puts them in the right places, so you can use them from VS Code. 


# What can I customize?

- I don't want the debugger to break in `start.S` every time!
    - Comment out `"stopAtConnect": true,` in the `launch.json` file
- I want to add some more custom test commands!
    - You can add them to the `testType` section of `inputs` in `tasks.json`
- That's about it honestly, if I think of something else I'll let you know

# Things that might go wrong
- `<some command>` doesn't exist
    - On Mac, see [Prerequisites](#prerequisites)
- gdb can't connect
    - If you started your kernel manually, consider using the test task. Otherwise, ensure you start your kernel while being in the os-compile directory. This is because the gdb socket opened by `sys161` is in the folder where you call `sys161` from, which is why the debugging guide required you to start both `sys161` and `cs350-gdb` in the same folder.
    - I've made gdb try to detect where you're running your sys161 instance from, but it's a bit difficult to account for all user cases.
- `setup.sh` is failing because the filepaths have spaces in them
    - Try quoting around your filepaths, using relative paths, or escaping your whitespace. 
- Permission denied with some `fatal: detected dubious ownership in repository at ...`
    - The instructions for the instructor's container uses sudo for some reason, which makes `root` the owner of the assignments repo. 
    - Try to make yourself the owner of the `assignments` folder with `sudo chown -R $(whoami) <cs350-container folder>/os161-container/assignments`
    - Let me know and we can try to figure it out
- Some docker permissioning error
    - Make sure docker is started
    - Give your (non-root) user permission to use docker by running `
 sudo groupadd docker && sudo usermod aG docker $USER`
    - also let me know!
- Lots of other things, but I would like to see your errors before I add them here

# Things I might do

- hotkey to start a kernel?? (done!)

# Why is there a license?

I took PD 10 and thought it would be a good idea. 